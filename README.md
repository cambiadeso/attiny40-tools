# ATTiny40 Tools

This repo provides a sample code to start developing a project with the ATTiny40 MCU


If you have available an USBasp device, you will need to update its firmware
before using it with this project. You can use a spare arduino by uploading
ArduinoAsISP firmware and following the instructions on [this page](https://www.electronics-lab.com/project/usbasp-firmware-update-guide/)

After this step, you will be able to upload code to the ATTiny40 using
USBasp as a TPI programmer with this wiring schema:

![wiring-schema-TPI](img/usbasp-wiring.png)

This repository provides a sample code for the ATTiny40 alongside with a
Makefile to help in the developing process.

More info available in [AVR Freaks](https://www.avrfreaks.net/comment/3117856#comment-3117856)

## Usage

To compile an .hex file

```bash
make
```

To upload the code to the ATTiny40

```bash
make flash
```

To clean build artifacts

```bash
make clean
```

## Requirements

You need avrdude installed in your computer, as well as avr-gcc compiler.
It is highly probable that your Linux distribution already has those
projects already packaged for you under similar names.

For Ubuntu 20.04 these packages seems to work fine:

```bash
sudo apt install gcc-avr avrdude
```

Feel free to submit a PR if something is missing

## Sample program

The `main.c` file in this repository blinks a LED that is wired to the PB2
pin of the ATTiny40. This is the wiring schema


![wiring-schema-TPI](img/sketch-wiring.png)


## Sample program ATTiny10

Looking for similar projects and assets I found the code in assembly for an
ATTiny10 family. I haven't tried if it works, as I don't know how to compile
it. Check it out here in `tinyblink.asm` file


## COPYRIGHT NOTICE

The only archive under WTFPL is Makefile, which is the one I've created. The
rest of them are not under a specific license, as I'm not sure the right author.

Feel free to file an issue if some right is violated in any of the rest of the 
files.