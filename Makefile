# Licensed under WTFPL - http://www.wtfpl.net/

MCU=attiny40
F_CPU=1200000
CC=avr-gcc
OBJCOPY=avr-objcopy
CFLAGS=-std=c99 -Wall -g -O2 -mmcu=${MCU} -DF_CPU=${F_CPU} -I.
TARGET=main
SRCS=main.c

all: compile

compile:
	${CC} ${CFLAGS} -o ${TARGET}.bin ${SRCS}
	${OBJCOPY} -j .text -j .data -O ihex ${TARGET}.bin ${TARGET}.hex
check:
	avr-size -C ${TARGET}.bin

flash: compile
	avrdude -p ${MCU} -c usbasp -U flash:w:${TARGET}.hex:i -F -P usb

show:
	cat ${TARGET}.hex    
        
clean:
	rm -f *.bin *.hex
